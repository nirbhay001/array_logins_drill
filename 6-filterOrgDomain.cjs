const data = require("./2-arrays-logins.cjs")

function filterOrgDomain() {
    let result = data.reduce((previous, current) => {
        if (current.email.endsWith("org")) {
            previous.push(current.email);
        }
        return previous;
    }, [])
    return result;
}
const result = filterOrgDomain();
console.log(result);
