const data = require("./2-arrays-logins.cjs")

function ipAdressComponent() {
    const dataResult = data.reduce((previous, current) => {
        let splitIp = current.ip_address.split(".");
        let result = splitIp.map((item) => {
            return Number(item);
        })
        current.ip_address = result;
        previous.push(current);
        return previous;
    }, [])
    return dataResult;
}
const result = ipAdressComponent();
console.log(result);


