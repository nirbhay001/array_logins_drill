const data=require("./2-arrays-logins.cjs")

function descendingOrder(){
    let resultData=data.sort((a,b)=>{
       if(a.first_name > b.first_name){
        return 1;
       }else if(a.first_name < b.first_name){
        return -1;
       }else{
        return 0;
       }
    });
    return resultData;
}

const result=descendingOrder();
console.log(result);