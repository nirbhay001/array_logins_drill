const data = require("./2-arrays-logins.cjs")

function fourthComponentIp() {
    let resultData = data.reduce((previous, current) => {
        let splitData = current.ip_address.split(".");
        previous += Number(splitData[3]);
        return previous;
    }, 0)
    return resultData;
}
const result = fourthComponentIp();
console.log(result);