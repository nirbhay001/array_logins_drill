const data = require("./2-arrays-logins.cjs")

function fullName() {
    let resultData = data.reduce((previous, current) => {
        let firstName = current.first_name;
        let lastName = current.last_name;
        current['fullName'] = firstName + " " + lastName;
        previous.push(current);
        return previous;
    }, [])
    return resultData;
}

const result = fullName();
console.log(result);