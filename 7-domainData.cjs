const data=require("./2-arrays-logins.cjs")

function domainData(){
    const resultData=data.reduce((previous,current)=>{
        let domainData=current.email.split(".");
        let domain=domainData[domainData.length-1];
        if(previous[domain]===undefined){
            previous[domain]=1;
        }
        else{
            previous[domain]++;
        }
        return previous;
    },{})
    return resultData;

}

const result=domainData();
console.log(result);
