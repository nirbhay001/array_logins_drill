const data = require("./2-arrays-logins.cjs")

function secondComponentIp() {
    let sum = 0;
    const resultData = data.reduce((previous, current) => {
        let splitData = current.ip_address.split(".");
        previous += Number(splitData[1]);
        return previous;
    }, 0)
    return resultData;

}

const result = secondComponentIp();
console.log(result);